FROM rekgrpth/gost
ADD entrypoint.sh /
CMD [ "ns-slapd" ]
ENV GROUP=389ds \
    USER=389ds
VOLUME "${HOME}"
RUN set -ex \
    && addgroup -S "${GROUP}" \
    && adduser -D -S -h "${HOME}" -s /sbin/nologin -G "${GROUP}" "${USER}" \
    && apk add --no-cache --virtual .build-deps \
        autoconf \
        automake \
        cracklib-dev \
        db-dev \
        file \
        g++ \
        gcc \
        gettext \
        gettext-dev \
        git \
        icu-dev \
        krb5-dev \
        libevent-dev \
        libtool \
        linux-pam-dev \
        make \
        musl-dev \
        net-snmp-dev \
        nspr-dev \
        nss-dev \
        openldap-dev \
        pcre-dev \
        rsync \
        zlib-dev \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && git clone --recursive https://github.com/RekGRpth/389-ds-base.git \
    && cd /usr/src/389-ds-base \
    && ./autogen.sh \
    && ./configure \
        --prefix=/usr/local \
        --with-openldap \
    && make -j"$(nproc)" install \
    && (strip /usr/local/bin/* /usr/local/lib/*.so || true) \
    && apk add --no-cache --virtual .389ds-rundeps \
        $(scanelf --needed --nobanner --format '%n#p' --recursive /usr/local | tr ',' '\n' | sort -u | grep -v libldaputil | grep -v libns-dshttpd | grep -v libnunc-stans | grep -v libsds | grep -v libslapd | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }') \
    && apk del --no-cache .build-deps \
    && rm -rf /usr/src /usr/local/share/doc /usr/local/share/man \
    && chmod +x /entrypoint.sh
