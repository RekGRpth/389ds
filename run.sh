#!/bin/sh

#docker build --tag rekgrpth/389ds . || exit $?
#docker push rekgrpth/389ds || exit $?
docker stop 389ds
docker rm 389ds
#docker pull rekgrpth/389ds || exit $?
docker volume create 389ds || exit $?
docker network create --opt com.docker.network.bridge.name=docker docker
docker run \
    --detach \
    --env GROUP_ID=$(id -g) \
    --env USER_ID=$(id -u) \
    --hostname 389ds \
    --name 389ds \
    --network docker \
    --restart always \
    --volume 389ds:/home \
    rekgrpth/389ds sh
#    rekgrpth/389ds ns-slapd -D /etc/dirsrv/slapd-dir
